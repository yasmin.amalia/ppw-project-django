from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import MyScheduleForm
from .models import MySchedule

# Create your views here.
def home(request):
    return render(request, 'home.html')

def about(request):
    return render(request, 'about.html')

def blog(request):
    return render(request, 'blog.html')

def gallery(request):
    return render(request, 'gallery.html')

def project(request):
    return render(request, 'project.html')

def contact(request):
    return render(request, 'contact.html')

def volunteer(request):
    return render(request, 'volunteer.html')

def guest(request):
    return render(request, 'guest.html')

response={}
def schedule_form(request):
    response['schedule_save'] = MyScheduleForm
    html = 'schedule_form.html'
    return render(request, html, response)

def schedule_save(request):
    form = MyScheduleForm(request.POST or None)
    if(request.method == 'POST'):
        response['name'] = request.POST['name'] if request.POST['name'] != "" else "Activity"
        response['category'] = request.POST['category']
        response['day'] = request.POST['day']
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['place'] = request.POST['place'] if request.POST['place'] != "" else "tba"
        
        sched = MySchedule(name=response['name'], category=response['category'], day=response['day'],
        date=response['date'], time=response['time'], place=response['place'])
        sched.save()
        
        return HttpResponseRedirect('/Schedule')
    else:
        return HttpResponseRedirect('')

def schedule(request):
    sched = MySchedule.objects.all()
    response['sched'] = sched
    html = 'schedule.html'
    return render(request, html, response)

def schedule_delete(request):
    sched = MySchedule.objects.all().delete()
    return HttpResponseRedirect('/Schedule')