from django.db import models
from django.utils import timezone

# Create your models here.
class MySchedule(models.Model):
    name = models.CharField(max_length=50)
    category = models.CharField(max_length=50)
    day = models.CharField(max_length=10)
    date = models.DateField()
    time = models.TimeField()
    place = models.CharField(max_length=50)