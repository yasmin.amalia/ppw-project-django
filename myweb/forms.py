from django import forms

class MyScheduleForm(forms.Form):
    error_message = {
        'required': 'Please fill in'
    }

    name_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Activity'
    }
    cat_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': '#Category'
    }
    day_attrs = {
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Day'
    }
    date_attrs = {
        'type': 'date',
        'class': 'form-control'
    }
    time_attrs={
        'type': 'time',
        'class': 'form-control'
    }
    place_attrs={
        'type': 'text',
        'class': 'form-control',
        'placeholder': 'Place'
    }
   
    name = forms.CharField(label='Activity name', required=True,
    widget=forms.TextInput(attrs=name_attrs))
    category = forms.CharField(label='Category', required=True, max_length=50,
    widget= forms.TextInput(attrs=cat_attrs))
    day = forms.CharField(label='Day', required=True, max_length=10,
    widget=forms.TextInput(attrs=day_attrs))
    date = forms.DateField(label='Date', required=True, widget=forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label='Time', required=True, widget=forms.TimeInput(attrs=time_attrs))
    place = forms.CharField(label='Place', required=True,
    widget=forms.TextInput(attrs=place_attrs))