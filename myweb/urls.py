"""PPWwebsite URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import path
from . import views
urlpatterns = [
    path('', views.home, name='Home'),
    path('About/', views.about, name='About'),
    path('Gallery/', views.gallery, name='Gallery'),
    path('Project/', views.project, name='Project'),
    path('Contact/', views.contact, name='Contact'),
    path('Volunteer/', views.volunteer, name='Volunteer'),
    path('Guest/', views.guest, name='Guest'),
    path('Schedule_Save/', views.schedule_save, name='Schedule_Save'),
    path('Schedule/', views.schedule, name='Schedule'),
    path('Schedule_Form/', views.schedule_form, name = 'Schedule_Form'),
    path('Scheduleclr/', views.schedule_delete, name='Schedule_Delete')
]
